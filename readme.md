# Введение

Привет! Ознакомься, лучше, с документацией полностью, чтобы продолжить.

## **Резюме** 
> Это [первый проект](https://bitbucket.org/ighosta9/summary) - резюме.

## **UI-Kit**

Это [второй проект](https://github.com/ighosta9/UI-Kit) - UI-Kit *(в процессе)*
> К нему ещё будут дополнительные страницы. Вскоре ссылка на репозиторий с ними появится, а документация обновится. Следите за обновлениями!

## Игра Жизнь

А это [третий проект](https://github.com/ighosta9/math-game) - игра Жизнь
> Ещё не начат. Следите за обновлениями!

## Единый репозиторий

А [здесь репозиторий](https://github.com/ighosta9/mainpages) со **всеми проектами**.
> Он нужен для хранения всех сайтов в одном репозитории. А единый репозиторий для того, чтобы работало на одном github.page, вместо нескольких

*P.S. Все репозитории, по мере ревью и до рефакторинга, будут обновляться, соответственно и документации к ним.*

# Начало работы

> **Установите все зависимости**, для возможности взаимодействия с проектом в целом:
 `npm install`


Теперь Вы можете использовать:
`npm run start` - запускает DevServer, с настройками:

`--env development`

Показывать только ошибки

Порт: 80

Автоматически открывать главную страницу в браузере


- `npm run build` - компилирует проект по пути: **/dist**. Настройка:
`--env production`

- `npm run server` - запускает локальный сервер, читая файлы **скомпилированного проекта**, по пути **/dist**

# Плагины в проекте
| Плагины | Версия |
| -- | -- |
| **[Webpack](https://webpack.js.org)** | **4@beta.3** |
| [Webpack CLI](https://github.com/webpack/webpack-cli) | 3.1.12 |
| [Webpack DevServer](https://webpack.js.org/configuration/dev-server) | 3.1.10 |
| [Webpack Merge](https://npmjs.com/package/webpack-merge) | 4.1.4 |
| [HTML Webpack](https://webpack.js.org/plugins/html-webpack-plugin) | 3.2.0 |
| [Extract Text Webpack](https://webpack.js.org/plugins/extract-text-webpack-plugin) | 4.0.0@beta.0 |
| [Clean Webpack](https://github.com/johnagan/clean-webpack-plugin) | 0.1.19 |
| [Node Static](https://npmjs.com/package/node-static) | 0.7.11 |
| [Node SASS](https://github.com/sass/node-sass) | 4.9.4 |
| [Pug](https://pugjs.org) | 2.0.3 |
| [Pug Loader](https://github.com/pugjs/pug-loader) | 2.4.0 |
| [SASS Loader](https://webpack.js.org/loaders/sass-loader) | 7.1.0 |
| [Style Loader](https://webpack.js.org/loaders/style-loader) | 0.23.1 |
| [CSS Loader](https://webpack.js.org/loaders/css-loader) | 1.0.1 |
| [PostCSS Loader](https://webpack.js.org/loaders/postcss-loader) | 3.0.0 |
| [File Loader](https://webpack.js.org/loaders/file-loader) | 2.0.0 |


# Режимы `--env`

Как Вы уже знаете:
`npm run start` использует `--env development`
`npm run build` использует  `--env production`

У нас существует переменная **common**, в которой существуют следующие плагины:

- [HTML Webpack](https://webpack.js.org/plugins/html-webpack-plugin)

- [webpack.LoaderOptionsPlugin](https://webpack.js.org/plugins/loader-options-plugin)

- [Clean Webpack](https://github.com/johnagan/clean-webpack-plugin)

- [Pug](https://pugjs.org)

- [Pug Loader](https://github.com/pugjs/pug-loader)


Тогда данные режимы используют следующие плагины:

| --env development | --env production |
| -- | -- |
| **common** | **common** |
| [Webpack DevServer](https://webpack.js.org/configuration/dev-server) | [Extract Text Webpack](https://webpack.js.org/plugins/extract-text-webpack-plugin) |
| [Style Loader](https://webpack.js.org/loaders/style-loader) | - |
| [CSS Loader](https://webpack.js.org/loaders/css-loader) | - |
| [SASS Loader](https://webpack.js.org/loaders/sass-loader) | - |

# Знакомство с файлами

Перед вами корневая файловая структура:

|
|-
|-src
|-webpack
|-.gitignore
|-README.md
|-package-lock.json
|-package.json
|-webpack.config.js
|-yarn.lock

Коротко пройдёмся, что здесь и как?

 - **src/** <br>
	--- bem *// Здесь хранятся БЭМ компоненты.*
    
	/common.blocks/ *// Здесь мы имеем стандартизацию БЭМ объектов.*
    
	/project.blocks/ *// Здесь изменение БЭМ объектов на проектный уровень.*
    
	/touch.blocks/ *// Здесь изменение БЭМ объектов по брейкпоинтам.*
	
    
	--- fonts *// Здесь хранятся шрифты*
fonts.scss *// А это файл конфигурации шрифтов. Если точнее, то их подключение*


	--- img *// Здесь мы храним изображения*
    
	
	--- scss *// Здесь главные SCSS стили, компилиуемые в CSS*
	
    
	--- index.pug *// Здесь главный PUG-код, компилируемый в HTML*
	
    
	--- index.js *// Здесь главный JS-код*
    
	
- **webpack/**

	--- css.extract.js *// Файл конфигурации [Extract Text Webpack](https://webpack.js.org/plugins/extract-text-webpack-plugin) плагина*

	--- devserver.js *// Файл конфигурации [Webpack DevServer'a](https://webpack.js.org/configuration/dev-server)*
	
	---file-loader.js *// Файл конфигурации [File Loader'a](https://webpack.js.org/loaders/file-loader)*

	---pug.js *// Файл конфигурации [Pug](https://pugjs.org) & [Pug Loader'a](https://github.com/pugjs/pug-loader)*

	---sass.js *// Файл конфигурации  [Style Loader'a](https://webpack.js.org/loaders/style-loader), [CSS Loader'a](https://webpack.js.org/loaders/css-loader),  [SASS Loader'a](https://webpack.js.org/loaders/sass-loader)*
