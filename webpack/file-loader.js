module.exports = function() {
  return {
    module: {
      rules: [
        // Изображения
        {
          test: /\.(jpg|png|svg|ico)$/,
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: './img/'
          },
        },
        // Шрифты
        {
          test: /\.(woff|ttf|eot)$/,
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: './fonts/'
          },
        },
      ],
    }
  }
};