module.exports = function() {
	return {
		devServer: {
			stats: 'errors-only',
				useLocalIp: false,
				open: true,
				port: 80
		}
	};
};