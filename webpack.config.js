const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const pug = require('./webpack/pug');
const devserver = require('./webpack/devserver');
const sass = require('./webpack/sass');
const extractCSS = require('./webpack/css.extract');
const fileloader = require('./webpack/file-loader');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const PATHS = {
  source: path.join(__dirname, 'src'),
  build: path.join(__dirname, 'dist')
};

const common = merge ([
  {
    entry: {'index': PATHS.source + '/index.js'},
    output: {
      path: PATHS.build,
      filename: '[name].js'
    },
    plugins: [
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: PATHS.source + '/index.pug'
      }),
      new webpack.LoaderOptionsPlugin({minimize: true}),
      new CleanWebpackPlugin(['dist'])
    ],
  },
  pug(),
  fileloader(),
]);

module.exports = function(env) {
  if (env === 'production') {
    return merge([
      common,
      extractCSS()
    ]);
  }
  if (env === 'development') {
    return merge([
      common,
      devserver(),
      sass(),
    ]);
  }
};